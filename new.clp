; template for person facts
(deftemplate person "person details"
    (slot name (type STRING))
    (slot age(type INTEGER))
    (slot gender (type STRING))
    (slot ethnicity (type STRING))
    (slot complexion (type STRING))
    (slot height (type SYMBOL))
    (slot sexuality (type STRING))
    (slot location (type STRING))
    (slot relationship-status (type STRING))
    (slot religion (type STRING))
    (slot hair-color(type STRING))
    (slot drinker (type STRING))
    (slot smoker (type STRING))
    )
    
;|-----------------------------Start Sexuality -----------------------------|
(defrule Sexuality
(not (Sexuality ?))
=>
(printout t "What is your Sexual orientation?"crlf"(straight male(SM), straight female(SF), Lesbian(L), Gay(G), Bisexual(B)) ")
(assert (Sexuality (read))))
;|-----------------------------End Sexuality -----------------------------|

;|-----------------------------Start Complexion -----------------------------|
; straight male 
(defrule complexion
(not (complexion ?))
(Sexuality SM)
=>
(printout t "What complexion do you like? "crlf " (Light skin(LS), Dark skin(DS))")
(assert (complexion (read))))

; straight female 
(defrule complexion
(not (complexion ?))
(Sexuality SF)
=>
(printout t "What complexion do you like? "crlf " (Light skin(LS), Dark skin(DS))")
(assert (complexion (read))))

; Lesbian
(defrule complexion
(not (complexion ?))
(Sexuality L)
=>
(printout t "What complexion do you like? "crlf " (Light skin(LS), Dark skin(DS))")
(assert (complexion (read))))

; Gay 
(defrule complexion
(not (complexion ?))
(Sexuality G)
=>
(printout t "What complexion do you like? "crlf " (Light skin(LS), Dark skin(DS))")
(assert (complexion (read))))

; Bisexual
(defrule complexion
(not (complexion ?))
(Sexuality BI)
=>
(printout t "What complexion do you like? "crlf " (Light skin(LS), Dark skin(DS))")
(assert (complexion (read))))

Transgender
(defrule complexion
(not (complexion ?))
(Sexuality TG)
=>
(printout t "What complexion do you like? "crlf " (Light skin(LS), Dark skin(DS))")
(assert (complexion (read))))


;|-----------------------------End Complexion -----------------------------|

;|-----------------------------Start Location -----------------------------|
; light skin
(defrule Location
(not (Location ?))
(complexion LS)
=>
(printout t "where do you prefer they lived  "crlf " ( Nairobi(N), Kisumu(K) ,Mombasa(M) )")
(assert (Location (read))))

; dark skin
(defrule Location
(not (Location ?))
(complexion DS)
=>
(printout t "where do you prefer they lived  "crlf " ( Nairobi(N), Kisumu(K) ,Mombasa(M) )")
(assert (Location (read))))
;|-----------------------------End Location -----------------------------|

;|-------------------- Heights ------------------------|
; height - Nairobi
(defrule Height
(not (Height ?))
(Location N)
=>
(printout t "What is your preferred height?  "crlf " ( Short(S), Average(A) ,Tall(T) )")
(assert (Height (read))))

; height - Mombasa
(defrule Height
(not (Height ?))
(Location M)
=>
(printout t "What is your preferred height?  "crlf " ( Short(S), Average(A) ,Tall(T) )")
(assert (Height (read))))

; height - Kisumu
(defrule Height
(not (Height ?))
(Location K)
=>
(printout t "What is your preferred height?  "crlf " ( Short(S), Average(A) ,Tall(T) )")
(assert (Height (read))))

;|-----------------------------End Height-----------------------------|

;|-----------------------------Start Body type -----------------------------|
; Body Type - Short
(defrule Body
(not (Body ?))
(Height S)
=>
(printout t "What is your preferred body type?  "crlf " ( Sturdy(S), Fit(F) ,BBW(B) )")
(assert (Body (read))))

; Body Type - Average
(defrule Body
(not (Body ?))
(Height A)
=>
(printout t "What is your preferred body type?  "crlf " ( Sturdy(S), Fit(F) ,BBW(B) )")
(assert (Body (read))))

; Body Type - Tall
(defrule Body
(not (Body ?))
(Height T)
=>
(printout t "What is your preferred body type?  "crlf " ( Sturdy(S), Fit(F) ,BBW(B) )")
(assert (Body (read))))

;|-----------------------------Start Body type -----------------------------|


















